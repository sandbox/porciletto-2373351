<?php

/**
 * @file
 * Hooks provided by the "Drush Job UI" module.
 */

/**
 * Provides a list of blocking jobs for the specified job.
 *
 * @param string $command
 *   The command identifying the job to be checked.
 *
 * @return string[]
 *   An array of commands whose execution should block the specified one.
 *   The "*" wildcard can be used to specify dynamic commands.
 */
function hook_djob_blocking_jobs($command) {
  switch ($command) {
    case 'foo1':
    case 'foo2':
      return array(
        'bar',
        'baz',
      );

    case 'bar':
    case 'baz':
      return array(
        'foo*',
      );

    default:
      return array();
  }
}
